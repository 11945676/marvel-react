import React, {useEffect, useState} from 'react';
import axios from 'axios';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import { Container,Row,Col,Table,Button,Modal,ModalHeader,ModalBody,ModalFooter, Card, CardBody, CardTitle, CardText } from 'reactstrap'
import { show_alerta } from '../function';

const ShowHeroes = () => {
    const url = 'http://localhost:5000/api/marvel';
    const [marvel,setMarvel]= useState([]);
    const [id,setId]= useState('');
    const [nombre,setNombre] = useState('');
    const [habilidades,setHabilidades]= useState('');
    const [raza,setRaza]= useState('');
    const [imagen,setImagen] = useState([]);
    const [edad,setEdad]= useState('');
    const [operation,setOperaciones]=useState(1);
    const [title,setTitle]= useState('');
    const [op,setOp]=useState('');

    
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);

 const openModal= () =>{
      setOp(1)
     //setDetalle(respuesta.results)
     setHabilidades('')
     //setImagenes(i)
     setEdad('')
      setRaza('')
      setNombre('')
     //console.log(respuesta.sprites.front_default)

    toggle();

 }
 


 
 const validar =(e)=>{
  e.preventDefault();
  var parametros;
  var metodo;
  if(nombre.trim() === ''){
    show_alerta('ESCRIBA EL NOMBRE DEL PERSONAJE','warning');
  }
  else if(habilidades.trim() === ''){
    show_alerta('ESCRIBA LA HABILIDAD DEL PERSONAJE','warning');
  }
  else if(raza.trim()=== ''){
    show_alerta('ESCRIBA LA RAZA DEL PERSONAJE','warning');
  }
  else if(imagen=== ''){
    show_alerta('EL CAMPO DE IMAGEN NO DEBE DE ESTAR VACÍO','warning');
  }
  else if(edad=== ''){
    show_alerta('ESCRIBA LA EDAD DEL PERSONAJE','warning');
  }
  else{
    let form = new FormData();
    form.append('nombre',nombre)
    form.append('habilidades',habilidades)
    form.append('raza',raza)
    form.append('imagen',imagen)
    form.append('edad',edad)
    var u;

    if(op==1){
        u= url
        metodo= 'POST';
    }
    else if(op==2){
      console.log(id);
      u=(url+'/'+id);
      metodo= 'PUT';
    }
    //enviarSolicitud((url+'/'+id),'DELETE',{});

    enviarSolicitud(u,metodo,form);
  }
 
 }
 const enviarSolicitud=async(ruta,metodo,parametros)=>{
  const config = {
    headers: { 'content-type': 'multipart/form-data'}
  }
  await axios ({method:metodo, url:ruta, data:parametros,config}).then(function(respuesta){
    //console.log(respuesta.status);
    if(respuesta.status == 200){
      
      show_alerta(respuesta.data.message,'success');
      console.log('sii');
      getMarvel();
    }
    document.getElementById('btnCerrar').click();
    
  })
  .catch(function(error){
   // show_alerta(error.response.data.errors,'error');
    console .log(error);
  });
 }

 const deletHeroe= (id,nombre) =>{
  const MySwal = withReactContent(Swal);
  MySwal.fire({
    title:'¿Seguro de eliminar '+nombre+'?',
    icon: 'question',text:'No se podrá dar macha atrás',
    showCancelButton:true,confirmButtonText:'Si, eliminar',cancelButtonText:'Cancelar'
  }).then((result)=>{
    if(result.isConfirmed){
      setId(id);
      enviarSolicitud((url+'/'+id),'DELETE',{});
    }
    else{
      show_alerta('El Heroe NO fue eliminado', 'info');
    }
  });
 }
 
  const getDetalle = (id,n,h,r,i,e) =>{
    setId(id)  
    setOp(2)
     //setDetalle(respuesta.results)
     setHabilidades(h)
     //setImagenes(i)
     setEdad(e)
      setRaza(r)
      setNombre(n)
     //console.log(respuesta.sprites.front_default)

    toggle();
  }

    useEffect( ()=>{
        getMarvel();
    },[]);

    const getMarvel = async () =>{
        const respuesta = await axios.get(url);
        setMarvel(respuesta.data.data);
    }

  return (
    <Container className='mt-4'>
      <Button onClick={()=>openModal()} className="col-md-5 offset-4 btn btn-dark"><i className='fa-solid fa-circle-plus mt-2'></i>    AGREGAR</Button>
    <Row className='mt-3'>
      <Col>
      <Card className='border border-warning shadow' >
        <CardTitle className='bg-warning'>
            <CardText className='h3 text-center'>M A R V E L</CardText>
        </CardTitle>
          <CardBody>
      <Table responsive bordered>
        <thead>
          <tr>
            <th>#</th>
            <th>NOMBRE</th>
            <th>HABILIDADES</th>
            <th>RAZA</th>
            <th>IMAGEN</th>
            <th>EDAD</th>
            <th></th>

          </tr>
        </thead>
        <tbody>
          {marvel.map((per,i)=>(
              <tr key={i}>
                <td>{(i+1)}</td>
                <td>{per.nombre}</td>
                <td>{per.habilidades}</td>
                <td>{per.raza}</td>
                <td>
                  <img src={"http://localhost:5000"+per.imagen} width={120} />
                </td>
                <td>{per.edad}</td>
                <td></td>
                <td>
                  <Button color='success' onClick={()=>{getDetalle(per._id,per.nombre,per.habilidades,per.raza,per.imagen,per.edad)}}> 
                  <i className="fa-solid fa-edit"></i>
                    </Button>
                    &nbsp;
                    <Button color='danger' onClick={()=>deletHeroe(per._id,per.nombre)}> 
                  <i className="fa-solid fa-trash"></i>
                    </Button>
                  </td>
              </tr>

          ))}
        </tbody>

      </Table>
      
      </CardBody>
      </Card>
      </Col>
    </Row>
    <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle} className='text-uppercase' >
          <p>PERSONAJE</p>
        </ModalHeader>
        <ModalBody>
            <input type="text" id='nombre' className='form-control mb-3' placeholder='NOMBRE' value={nombre} onChange={(e)=>setNombre(e.target.value)}/>
            <input type="text" id='habiliades' className='form-control mb-3' placeholder='HABILIADES' value={habilidades} onChange={(e)=>setHabilidades(e.target.value)}/>
            <input type="text" id='raza' className='form-control mb-3' placeholder='RAZA' value={raza} onChange={(e)=>setRaza(e.target.value)}/>
            <input type="text" id='edad' className='form-control mb-3' placeholder='EDAD' value={edad} onChange={(e)=>setEdad(e.target.value)}/>
            <input type="file" id='imagen' className='form-control mb-3' placeholder='IMG' onChange={(e)=>setImagen(e.target.files[0])}/>
            <Button color='dark' onClick={validar}><i><i className="fa-regular fa-floppy-disk"></i></i></Button>
        </ModalBody>
        <ModalFooter>
          <Button type='button' className='btn btn-secondary' id='btnCerrar' data-bs-dismiss='modal' onClick={toggle}>
            CERRAR
          </Button>
        </ModalFooter>
      </Modal>
   </Container>

  )
}

export default ShowHeroes