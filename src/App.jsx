import {Routes, Route, BrowserRouter } from "react-router-dom";
import ShowHeroes from './components/ShowHeroes';


function App() {
  //const [count, setCount] = useState(0)

  return (
  <BrowserRouter>
  <Routes>
    <Route path='/' element={<ShowHeroes></ShowHeroes>}></Route>
  </Routes>
  </BrowserRouter>
  )
}

export default App
